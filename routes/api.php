<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'middleware' => 'apilogger'], function () {
    // Bank Routes
    Route::resource('bank', 'Api\V1\BankController');

    // Bank Accounts Routes
    Route::resource('bank-account', 'Api\V1\BankAccountsController');

    // Transactions Routes
    Route::resource('transactions', 'Api\V1\TransactionsController');

    // Bank postal addreess Route
    Route::get('bank-postal-address', 'Api\V1\BankController@postalAddress');
});