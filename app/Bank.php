<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'address'];

    /**
     * Add Postal Code attribute.
     *
     * @return bool
     */
    protected $appends = ['postal_address'];

    /**
     * Get the bank account(s) for the single bank.
     */
    public function bankAccounts()
    {
        return $this->hasMany('App\BankAccounts');
    }

    /**
     * Get the postal address bank.
     */
    public function getPostalAddressAttribute()
    {
        return $this->attributes['postal_address'] = $this->name . ' ' . $this->address;
    }

}
