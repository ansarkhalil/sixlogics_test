<?php

namespace App\Providers;

use App\Observers\TransactionsObserver;
use App\Transactions;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Transactions::observe(TransactionsObserver::class);
    }
}
