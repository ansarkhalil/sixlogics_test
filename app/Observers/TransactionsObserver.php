<?php

namespace App\Observers;

use App\BankAccounts;
use App\Transactions;

class TransactionsObserver
{
    /**
     * Handle the transactions "created" event.
     *
     * @param \App\Transactions $transactions
     * @return void
     */
    public function created(Transactions $transactions)
    {
        if ($transactions->transaction_type == 'deposit') {

            // Withdraw transaction from source account
            $new_transaction = new Transactions();

            $new_transaction->account_id = $transactions->account_id_source;
            $new_transaction->amount = -1 * abs($transactions->amount);
            $new_transaction->transaction_type == 'withdraw';
            $new_transaction->account_id_source = $transactions->account_id;

            // Update  Account Balance
            $account = BankAccounts::find($transactions->account_id);

            $account->balance += $transactions->amount;
            $account->save();

            if ($new_transaction->save()) {

                // Update Source Account Balance
                $account = BankAccounts::find($transactions->account_id_source);

                $account->balance -= $transactions->amount;
                $account->save();

                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Handle the transactions "updated" event.
     *
     * @param \App\Transactions $transactions
     * @return void
     */
    public function updated(Transactions $transactions)
    {
        //
    }

    /**
     * Handle the transactions "deleted" event.
     *
     * @param \App\Transactions $transactions
     * @return void
     */
    public function deleted(Transactions $transactions)
    {
        //
    }

    /**
     * Handle the transactions "creating" event.
     *
     * @param \App\Transactions $transactions
     * @return boolean
     */
    public function creating(Transactions $transactions)
    {
        if ($transactions->transaction_type == 'deposit') {

            $sourceBalance = BankAccounts::find($transactions->account_id_source)->balance;

            if ($sourceBalance >= $transactions->amount) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Handle the transactions "force deleted" event.
     *
     * @param \App\Transactions $transactions
     * @return void
     */
    public function forceDeleted(Transactions $transactions)
    {
        //
    }
}
