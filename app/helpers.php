<?php

/**
 * Display failure Response.
 *
 * @return Json
 */
function failureResponse($data = null)
{
    $arr['message'] = (getenv('APP_DEBUG')) ? $data : 'Something Went Wrong';
    return response()->json($arr, 500);
}

/**
 * Display Success Response.
 *
 * @return Json
 */
function validationFailed($data )
{
    return response()->json($data, 422);
}