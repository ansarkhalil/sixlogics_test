<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['transaction_type','account_id_source','account_id','amount'];

    /**
     * Get the account that owns the transaction.
     */
    public function bankAccount()
    {
        return $this->belongsTo('App\BankAccounts');
    }

}
