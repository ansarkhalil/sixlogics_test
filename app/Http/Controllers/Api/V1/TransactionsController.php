<?php

namespace App\Http\Controllers\Api\V1;

use App\BankAccounts;
use App\Http\Controllers\Controller;
use App\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return Transactions::all();
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required|exists:bank_accounts,id',
            'amount' => 'required|numeric',
            'account_id_source' => 'required|exists:bank_accounts,id'
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }
        try {
            $transaction = new Transactions();

            $transaction->account_id = $request->account_id;
            $transaction->amount = $request->amount;
            $transaction->transaction_type = 'deposit';
            $transaction->account_id_source = $request->account_id_source;

            if ($transaction->save()) {
                return response()->json($transaction, 201);
            } else {
                return validationFailed(['amount'=>"Source Account Doesn't have enough balance"]);
            }
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return Transactions::find($id);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }
}
