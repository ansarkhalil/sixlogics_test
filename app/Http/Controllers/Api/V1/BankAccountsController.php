<?php

namespace App\Http\Controllers\Api\V1;

use App\BankAccounts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;

class BankAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return BankAccounts::with('transactions')->get();
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_number' => 'required|Integer|unique:bank_accounts',
            'balance' => 'required|numeric',
            'bank_id' => 'required|exists:banks,id'
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }
        try {
            return response()->json(BankAccounts::create($request->all()),201);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return BankAccounts::find($id);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arr = $request->all();
        $arr['id'] = $id;

        $validator = Validator::make($arr, [
            'id' => 'required|exists:bank_accounts',
            'account_number' => 'required|Integer|unique:bank_accounts',
            'balance' => 'required|numeric',
            'bank_id' => 'required|exists:banks,id'
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }
        try {
            $bankAccount = BankAccounts::findOrFail($id);
            $bankAccount->update($request->all());

            return $bankAccount;
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|exists:bank_accounts',
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }

        try {
            BankAccounts::find($id)->delete();

            return response()->json(null, 204);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }
}
