<?php

namespace App\Http\Controllers\Api\V1;

use App\Bank;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return Bank::with('bankAccounts')->get();
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Display a postal address of the bank.
     *
     * @return \Illuminate\Http\Response
     */
    public function postalAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:100|unique:banks',
            'address' => 'required|min:10'
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }
        try {
            $bank = Bank::create($request->all());
            return response()->json(['postal_code'=>$bank->postal_address], 201);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:100|unique:banks',
            'address' => 'required|min:10'
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }
        try {
            return response()->json(Bank::create($request->all(), 201));
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|exists:banks',
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }

        try {
            return Bank::with('bankAccounts')->find($id);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        Request $request,
        $id
    ) {
        $arr = $request->all();
        $arr['id'] = $id;

        $validator = Validator::make($arr, [
            'id' => 'required|exists:banks',
            'name' => 'required|min:2|max:100|unique:banks',
            'address' => 'required|min:10'
        ]);

        if ($validator->fails()) {
            return validationFailed($validator->errors()->getMessages());
        }

        try {
            $bank = Bank::findOrFail($id);
            $bank->update($request->all());

            return $bank;
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        $id
    ) {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|exists:banks',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 422);
        }
        Bank::find($id)->delete();

        try {
            return response()->json(null, 204);
        } catch (\Exception $ex) {
            return failureResponse($ex->getMessage());
        }
    }
}
