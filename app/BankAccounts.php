<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccounts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['account_number','balance','bank_id'];

    /**
     * Get the bank that owns the account.
     */
    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    /**
     * Get the transaction(s) for the single account.
     */
    public function transactions()
    {
        return $this->hasMany('App\Transactions','account_id','id');
    }
}
