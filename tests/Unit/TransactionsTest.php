<?php

namespace Tests\Unit;

use App\Bank;
use App\BankAccounts;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionsTest extends TestCase
{
    /**
     * A basic unit test to make a transactions.
     *
     * @return void
     */
    public function testStore()
    {
        $faker = \Faker\Factory::create();

        $bank_asc = BankAccounts::orderBy('id', 'ASC')->first();
        $bank_desc = BankAccounts::orderBy('balance', 'DESC')->first();
        //var_dump($bank_desc->id);
        //exit();
        $data = [
            'account_id' => $bank_asc->id,
            'amount' => 10,
            'transaction_type' => 'deposit',
            'account_id_source'=> $bank_desc->id

        ];
        // $expected = ['postal_code'=>$data['name']." ".$data['address']];

        $response = $this->json('Post', '/api/v1/transactions/',$data );
        $response->assertStatus(201);
        $response->assertJsonStructure(["account_id","amount","transaction_type","account_id_source","updated_at","created_at","id"]);
    }
}
