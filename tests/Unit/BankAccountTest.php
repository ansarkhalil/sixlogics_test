<?php

namespace Tests\Unit;

use App\Bank;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BankAccountTest extends TestCase
{
    /**
     * Test Account adding to bank
     *
     * @return void
     */
    public function testStore()
    {
        $faker = \Faker\Factory::create();

        $bank_id = Bank::first()->id;

        $data = [
            'account_number' => $faker->bankAccountNumber,
            'balance' => 1,
            'bank_id'=> $bank_id

        ];
       // $expected = ['postal_code'=>$data['name']." ".$data['address']];

        $response = $this->json('Post', '/api/v1/bank-account',$data );
        $response->assertStatus(201);
        $response->assertJsonStructure(["account_number","balance","updated_at","created_at","id"]);
    }
}
