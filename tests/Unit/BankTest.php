<?php

namespace Tests\Unit;

use App\Bank;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

if (!defined('LARAVEL_START')) {
    define('LARAVEL_START', microtime(true));
}
class BankTest extends TestCase
{
    /**
     * Test Bank Postal Address
     *
     * @return void
     */
    public function testPostalAddress()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'name' => $faker->company,
            'address' => $faker->address,

        ];
        $expected = ['postal_code'=>$data['name']." ".$data['address']];

        $response = $this->json('Get', '/api/v1/bank-postal-address',$data );
        $response->assertStatus(201);
        $response->assertJson($expected,false);
    }
}
