<?php

use App\Bank;
use App\BankAccounts;
use Illuminate\Database\Seeder;

class BankAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        BankAccounts::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few bank account numbers in our database:
        foreach (Bank::all() as $bank) {
            BankAccounts::create([
                'account_number' => $faker->bankAccountNumber,
                'balance' => 100,
                'bank_id' => $bank->id,
            ]);
        }
    }
}
