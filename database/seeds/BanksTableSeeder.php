<?php

use App\Bank;
use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Bank::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few banks in our database:
        for ($i = 0; $i < 50; $i++) {
            Bank::create([
                'name' => $faker->company,
                'address' => $faker->address,
            ]);
        }
    }
}
