<?php

use App\Bank;
use App\BankAccounts;
use App\Transactions;
use Illuminate\Database\Seeder;

class TransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Transactions::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few bank account numbers in our database:
        foreach (BankAccounts::all() as $account) {
            Transactions::create([
                'transaction_type' => 'default',
                'account_id' => $account->id,
                'amount' => 100,
                'account_id_source' => $account->id,
            ]);
        }
    }
}
